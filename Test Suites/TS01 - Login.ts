<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS01 - Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>c4649101-8152-455a-af3c-56cd6de79b7f</testSuiteGuid>
   <testCaseLink>
      <guid>b34f0312-82f0-45cf-af11-54deb5306f56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/S01 - Login/TC01 - Login with valid username and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a40b7fef-8b97-49a7-b19f-1bc4dcf106ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/S01 - Login/TC02 - Login with valid username and invalid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>06fdafbc-b27d-42ff-a8da-00a5a1dd9eb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/S01 - Login/TC03 - Login with invalid username and valid password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a9f9ba33-9294-42b6-86f0-860bd55383ec</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DF01 - Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>a9f9ba33-9294-42b6-86f0-860bd55383ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>3afa12ac-03e3-4b94-b4e0-14ba03ec3e21</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a9f9ba33-9294-42b6-86f0-860bd55383ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>727cb02a-f987-4ec4-916d-ed377c3f067e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4cb4b1b8-227a-4a05-af5e-ac74f582bba9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/S01 - Login/TC04 - Login with invalid username and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d7aac746-ff85-410d-bcdd-7b2d0c9f6527</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/S01 - Login/TC05 - Login without anything</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
